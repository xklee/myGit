#### Git常用命令 - 基础
```
git init # 把目录变成Git可以管理的仓库
git add av.txt # 把av.txt添加到仓库
git add -A # 把所有文件提交（在根目录下执行）
git commit -m "注释" # 把文件提交到仓库
git status # 仓库当前的状态
git diff xxx.txt # 可以查看修改内容
git log (--pretty=oneline) # 命令显示从最近到最远的提交日志
git reset --hard HEAD^ # 回退到上一个版本(上上一个版本就是HEAD^^，往上100个版本写成HEAD~100)
git reset --hard commit_id # 指定回到未来的某个版本
git reflog # 用来记录你的每一次命令
git checkout -- file(readme.txt) # 可以丢弃工作区的修改，让这个文件回到最近一次git commit或git add时的状态
git reset HEAD file # 可以把暂存区的修改撤销掉（unstage），重新放回工作区
git rm file # 用于删除一个文件
```

#### Git常用命令 - 分支
```
git branch # 查看分支
git branch <name> # 创建分支
git checkout <name> # 切换分支
git checkout -b <name> # 创建+切换分支
git merge <name> # 合并某分支到当前分支
# 将dev分支合并到master. --no-ff参数，表示禁用Fast forward
# Fast forward模式下，删除分支后，会丢掉分支信息
git merge --no-ff -m "merge with no-ff" dev
git branch -d <name> # 删除分支
git branch -D <name> # 丢弃一个没有被合并过的分支，强行删除
git log --graph # 可以看到分支合并图


git stash 把当前工作现场“储藏”起来，等以后恢复现场后继续工作
git stash list 工作现场列表
git stash pop 恢复工作现场，恢复的同时把stash内容也删了
git stash apply 恢复工作现场，恢复后，stash内容并不删除
git stash apply stash@{0} 恢复指定的stash
git stash drop 删除stash内容

```

#### Git常用命令 - 远程协作
```
# 远程协作第一步：创建SSH Key，会在C:\Users\xkli\.ssh生成文件
ssh-keygen -t rsa -C "iam_leexk@163.com"
#  远程协作第二步：“Add SSH Key”
# 自报家门
git config (--global) user.name "iron_will"
git config (--global) user.email "iam_leexk@163.com"
# 本地库关联一个远程库
git remote add origin git@git.oschina.net:iron_will/test.git
# 更改关联的远程仓库地址
git remote set-url [--push] <name> <newurl> [<oldurl>]
git remote set-url --add <name> <newurl>
git remote set-url --delete <name> <url>
eg：git remote set-url origin git@git.oschina.net:iron_will/test.git

# 推送本地库内容到远程
git push -u origin master 
# 查看远程库的信息
git remote -v
# Git推送分支
git push origin master # 把主干分支push到远程库
git push origin dev # 把dev分支push到远程库
```

#### Git常用命令 - 标签篇
```
git tag <tag_name> # 在commit_id 为 HEAD 的版本，新建一个标签。
git tag <tag_name> commit_id # 为版本为commit_id 的版本，新建一个标签
git tag -a v0.1 -m "version 0.1 released" 3628164(commit_id) # 用-a指定标签名，-m指定说明文字。
git tag -s v0.2 -m "signed version 0.2 released" fec145a(commit_id) # 通过-s用私钥签名一个标签
git tag # 查看标签列表，列表按名字排列
git show <tag_name> # 查看标签信息
git tag -d <tag_name> # 删除标签
git push origin <tag_name> # 推送某个标签到远程
git push origin --tags # 一次性推送全部尚未推送到远程的本地标签
# 如果标签已经推送到远程，要删除远程标签就麻烦一点，先从本地删除：
git tag -d <tag_name>
# 然后，从远程删除。删除命令也是push，但是格式如下：
git push origin :refs/tags/<tag_name>

#### Git学习地址
> http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000

#### Git工具下载
> https://git-for-windows.github.io/